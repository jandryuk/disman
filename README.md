---

DBus configuration for Disman 2.0  

---

If you are trying to get this to run on a non-secureview box, you'll need to install the DBus config file.

sudo cp -f dbus-1/mil.af.secureview.disman.conf /etc/dbus-1/system.d/  
sudo /etc/init.d/dbus restart

